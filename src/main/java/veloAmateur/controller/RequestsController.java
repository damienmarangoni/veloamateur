package veloAmateur.controller;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;
import veloAmateur.data.Race;
import veloAmateur.dto.RaceDTO;
import veloAmateur.services.RaceService;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/veloamateur")
public class RequestsController {

    @Autowired
    private RaceService raceService;

    @Autowired
    private ModelMapper modelMapper;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    @ResponseBody
    public String getDefault() {

        return "Hello";
    }
    //titi
    @RequestMapping(value = "/race/{id}", method = RequestMethod.GET)
    @ResponseBody
    public RaceDTO getRace(@PathVariable("id") Long id) {

        return convertToDto(raceService.getRaceById(id));
    }
    //tata
    private RaceDTO convertToDto(Race race) {
        RaceDTO raceDto = modelMapper.map(race, RaceDTO.class);
        return raceDto;
    }


    @RequestMapping(value = "/races", method = RequestMethod.GET)
    @ResponseBody
    public List<RaceDTO> getAllRaces() {
        int page = 1;
        int size = 1;
        String sortDir = "desc";
        String sort = "id";

        List<Race> races = raceService.getRacesList(page, size, sortDir, sort);
        return races.stream().map(race -> convertToDto(race)).collect(Collectors.toList());
    }

    private List<RaceDTO> convertToDtos(List<Race> races) {
        Type listType = new TypeToken<List<RaceDTO>>() {}.getType();
        List<RaceDTO> raceDtos = modelMapper.map(races, listType);
        return raceDtos;
    }
}