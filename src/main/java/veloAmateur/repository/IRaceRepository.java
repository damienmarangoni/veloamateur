package veloAmateur.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import veloAmateur.data.Race;

@Repository
public interface IRaceRepository extends JpaRepository<Race, Long> {

    Page<Race> findAll(Pageable pageable);

    Race findById(Long id);
}
