package veloAmateur.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import veloAmateur.data.User;

@Repository
public interface IUserRepository extends JpaRepository<User, Long> {

    Page<User> findAll(Pageable pageable);
}
