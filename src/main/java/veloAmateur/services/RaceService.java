package veloAmateur.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import veloAmateur.data.Race;
import veloAmateur.repository.IRaceRepository;

import java.util.List;

@Service
public class RaceService {

    @Autowired
    private IRaceRepository racesRepository;

    public List<Race> getRacesList(
            int page, int size, String sortDir, String sort) {

        //PageRequest pageReq
        //        = new PageRequest(page, size, Sort.Direction.fromString(sortDir), sort);

        //Page<Race> races = racesRepository
        //        .findAll(pageReq);
        List<Race> races = racesRepository
                .findAll();
        //return races.getContent();
        return races;
    }

    public Race getRaceById(Long id) {

        Race race = racesRepository.findById(id);
        return race;
    }
}
