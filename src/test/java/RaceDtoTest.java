import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import veloAmateur.Application;
import veloAmateur.data.Race;
import veloAmateur.dto.RaceDTO;
import veloAmateur.repository.IRaceRepository;

import java.lang.reflect.Type;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = Application.class)
@TestPropertySource(
        locations = "classpath:application-test.properties")
public class RaceDtoTest {

    private ModelMapper modelMapper = new ModelMapper();

    @Test
    public void whenConvertRaceEntityToRaceDto_thenCorrect() {
        Race myRace = new Race();
        myRace.setDate("20161023");
        myRace.setPlace("Ludon-Mèdoc");
        myRace.setFederation("UFOLEP");
        myRace.setType("CYCLOCROSS");
        myRace.setSection("TOUTES CATEGORIES");

        RaceDTO raceDto = modelMapper.map(myRace, RaceDTO.class);
        Assert.assertEquals(myRace.getId(), raceDto.getId());
        Assert.assertEquals(myRace.getDate(), raceDto.getDate());
        Assert.assertEquals(myRace.getPlace(), raceDto.getPlace());
    }

    @Autowired
    private IRaceRepository racesRepository;

    @Test
    public void whenConvertRaceEntitiesToRaceDtos_thenCorrect() {
        List<Race> myRacesList = racesRepository.findAll();

        Type listType = new TypeToken<List<RaceDTO>>() {}.getType();
        List<RaceDTO> raceDtos = modelMapper.map(myRacesList, listType);

        Assert.assertEquals(2, raceDtos.size());
    }
}
