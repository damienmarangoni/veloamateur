import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import veloAmateur.Application;
import veloAmateur.data.Race;
import veloAmateur.repository.IRaceRepository;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = Application.class)
@TestPropertySource(
        locations = "classpath:application-test.properties")
public class RaceRepositoryTest {

    @Autowired
    private IRaceRepository racesRepository;


    @Test
    public void findAllRaces() {

        Race myRace = new Race();
        myRace.setDate("20161023");
        myRace.setPlace("Ludon-Medoc");
        myRace.setFederation("UFOLEP");
        myRace.setType("CYCLOCROSS");
        myRace.setSection("TOUTES CATEGORIES");

        List<Race> races = racesRepository.findAll();

//        System.out.println("=================START: Races list===================");
//        races.forEach(System.out::println);
//        System.out.println("=================END: Races list===================");

        Assert.assertNotNull(races.size());
        Assert.assertEquals(myRace.getDate(), races.get(1).getDate());
        Assert.assertEquals(myRace.getPlace(), races.get(1).getPlace());
        Assert.assertEquals(myRace.getFederation(), races.get(1).getFederation());
        Assert.assertEquals(myRace.getType(), races.get(1).getType());
        Assert.assertEquals(myRace.getSection(), races.get(1).getSection());
    }
}
