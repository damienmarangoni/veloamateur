import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import veloAmateur.Application;
import veloAmateur.data.User;
import veloAmateur.repository.IUserRepository;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = Application.class)
@TestPropertySource(
        locations = "classpath:application-test.properties")
public class UserRepositoryTest {

    @Autowired
    private IUserRepository usersRepository;


    @Test
    public void findAllUsers() {

        User myUser = new User();
        myUser.setName("Damien Marangoni");
        myUser.setEmail("damienmarangoni@gmail.com");
        myUser.setPassword("password");

        List<User> users = usersRepository.findAll();

//        System.out.println("=================START: Races list===================");
//        users.forEach(System.out::println);
//        System.out.println("=================END: Races list===================");

        Assert.assertNotNull(users.size());
        Assert.assertEquals(myUser.getName(), users.get(0).getName());
        Assert.assertEquals(myUser.getEmail(), users.get(0).getEmail());
        Assert.assertEquals(myUser.getPassword(), users.get(0).getPassword());
    }
}
