DROP TABLE users IF EXISTS;
CREATE TABLE users(id INT(11) NOT NULL DEFAULT '0', name VARCHAR(255), email VARCHAR(255), password VARCHAR(255));
DROP TABLE races IF EXISTS;
CREATE TABLE races( id INT(11) NOT NULL DEFAULT '0',
                    date VARCHAR(255), 
                    place VARCHAR(255), 
                    federation VARCHAR(255), 
                    type VARCHAR(255), 
                    section VARCHAR(255),
                    PRIMARY KEY (`id`));
--CREATE TABLE races_details( id INT(11) NOT NULL DEFAULT '0',
--                            place VARCHAR(255),
--                            type VARCHAR(255),
--                            federation VARCHAR(255),
--                            section VARCHAR(255),
--                            organizer VARCHAR(255),
--                            number_time VARCHAR(255),
--                            start_time VARCHAR(255),
--                            contact VARCHAR(255),
--                            phone_number VARCHAR(255),
--                            PRIMARY KEY (id),
--                            FOREIGN KEY (id) REFERENCES races (id));